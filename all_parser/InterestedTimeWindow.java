import java.util.Scanner;
import java.io.File;

public class InterestedTimeWindow{
	public static void main(String []args) throws Exception{
		Scanner scanner = null;
		
		if(args.length != 2){
			System.err.println("Usage: \njava InterestedTimeWindow input_file category");
		}
		String intputFile = args[0]; //the input file path
		String category = args[1];

		try{
			File file = new File(intputFile);
			scanner = new Scanner(file);
		}
		catch(Exception e){
			System.err.println("File open error");
			System.exit(1);
		}

		double start = 0.0;
		double end = 0.0;
		boolean firstLine = false;
		int slice = 1;

		Scanner s1 = new Scanner(System.in);
		while(scanner.hasNext()){
			String line = scanner.nextLine();
			if(line.contains("#              | |        |      |   ||||       |         |")) firstLine = true;

			if(!firstLine) continue;

			if(category.equals("idle")){
				if(Double.compare(start,0.0) == 0){
					start = s1.nextDouble();
						//continue;
				}
					
				if(Double.compare(end,0.0) == 0){
					end = s1.nextDouble();
				}

				if(Double.compare(start,0.0) > 0 && Double.compare(end,0.0) > 0){
					//System.out.println("I am here!");
					if(line.contains("cpu_idle:")){
						while(line.contains("  ")) line = line.replace("  ", " ");
						String []temp = line.split(" ");
						if(Double.parseDouble(temp[5].substring(0, temp[5].length()-1)) > start && Double.parseDouble(temp[5].substring(0, temp[5].length()-1)) < end){
							if(line.contains("state=4294967295")){
								System.out.println("End at: "+Double.parseDouble(temp[5].substring(0, temp[5].length()-1)));
							}
							else{
								System.out.println("Idle period: "+slice);
								System.out.println("Start at: " + Double.parseDouble(temp[5].substring(0, temp[5].length()-1)));
								++slice;
							}
						}
					}
					continue;
				}

				/*if(s1.hasNext()){
					
					else{
						start = end;
						end = s1.nextDouble();
					}
				}*/
				
			}
			else if(category.equals("launching")){
				if(line.contains("wq:Window") && line.contains("|1")){
					String []temp = line.split(" ");
					for(int i = 0; i < temp.length; i++){
						if(temp[i].contains(".")&&temp[i].contains(":")){
							start = Double.parseDouble(temp[i].substring(0,temp[i].length()-1));
							
							break;
						}
					}
				}
				else if(line.contains("|launching|") && line.contains("S|")){
					String []temp = line.split(" ");
					for(int i = 0; i < temp.length; i++){
						if(temp[i].contains(".")&&temp[i].contains(":")){
							end = Double.parseDouble(temp[i].substring(0,temp[i].length()-1));
							System.out.println("Start time: "+start);
							System.out.println("Stage 1: "+end);
							break;
						}
					}

				}
				else if(line.contains("|launching|") && line.contains("F|")){
					String []temp = line.split(" ");
					for(int i = 0; i < temp.length; i++){
						if(temp[i].contains(".")&&temp[i].contains(":")){
							end = Double.parseDouble(temp[i].substring(0,temp[i].length()-1));
							System.out.println("Stage 2: "+end);
							break;
						}
					}

				}
			}
		}

		
		//System.out.println("End time: "+end);
	}
}