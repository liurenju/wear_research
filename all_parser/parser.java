import java.util.Scanner;
import java.io.PrintWriter;

public class parser{
	public static void main(String []args) throws Exception{
		String line[] = new String[7];
		Scanner scanner = new Scanner(System.in);
		PrintWriter pw = new PrintWriter("Sent.txt");
		PrintWriter pw2 = new PrintWriter("Received.txt");
		while(scanner.hasNext()){
			line = scanner.nextLine().split(",");
			if(!line[0].contains("NO")){// || line[3].contains("Nexus")){
				if(line[2].contains("LGE-W1-0052")){
					System.out.println(line[1].substring(1, line[1].length()-1) +'\t'+line[5].substring(1, line[5].length()-1) + '\t' + (line[2].contains("Nexus") ? ("Nexus"+'\t'+ "Watch") : ("Watch" + '\t' + "Nexus")));
					pw.println(line[1].substring(1, line[1].length()-1) +'\t'+line[5].substring(1, line[5].length()-1) + '\t' + (line[2].contains("Nexus") ? ("Nexus"+'\t'+ "Watch") : ("Watch" + '\t' + "Nexus")));
				}
				else if(line[2].contains("Nexus")){
					System.out.println(line[1].substring(1, line[1].length()-1) +'\t'+line[5].substring(1, line[5].length()-1) + '\t' + (line[2].contains("Nexus") ? ("Nexus"+'\t'+ "Watch") : ("Watch" + '\t' + "Nexus")));
					pw2.println(line[1].substring(1, line[1].length()-1) +'\t'+line[5].substring(1, line[5].length()-1) + '\t' + (line[2].contains("Nexus") ? ("Nexus"+'\t'+ "Watch") : ("Watch" + '\t' + "Nexus")));
				}
			}
		}
		pw.close();
		pw2.close();
	}
}