import java.util.Scanner;
import java.util.Hashtable;
import java.io.File;

public class BinderParser{
	public static void main(String []args) throws Exception{
		String filePath = args[0];
		File file = new File(filePath);
		Scanner scanner = null;
		Scanner scanner1 = new Scanner(System.in);
		long startFilter = 0;
		long endFilter = 1;
		//System.out.println("Please enter start time: ");
		startFilter = Long.parseLong(args[1]);
		//System.out.println("Please enter end time: ");
		endFilter = Long.parseLong(args[2]);

		try{
			scanner = new Scanner(file);
		}
		catch (Exception e){
			System.err.println("File not found");
			System.exit(1);
		}

		Hashtable<Integer, String> transactions = new Hashtable<Integer, String> ();

		while(scanner.hasNext()){
			String line = scanner.nextLine();
			if(line.contains("binder_transaction:")){
				String parsed[] = line.split(" ");
				int id = Integer.parseInt(parsed[parsed.length-7].substring(12));
				transactions.put(id, line);
				continue;
			}
			else if(line.contains("binder_transaction_received:")){
				String parsed[] = line.split(" ");
				int id = Integer.parseInt(parsed[parsed.length-1].substring(12, parsed[parsed.length-1].indexOf("\\")));
				//System.out.println("Id: " + id);
				if(transactions.get(id) == null){
					System.err.println("Error! Binder received before sent!");
					System.err.println(line);
					System.exit(1);
				}

				String source = transactions.get(id);
				String source_parsed[] = source.split(" ");
				String dest = parsed[0];
				String src = source_parsed[0];

				int index = 1;
				while(dest.startsWith(" ") || (dest.equals(""))){
					dest = parsed[index];
					index++;
				}

				while(!parsed[index].contains("(")){
					dest = dest +" "+ parsed[index];
					++index;
				}

				index = 1;

				while(src.startsWith(" ") || (src.equals(""))){
					src = source_parsed[index];
					index++;
				}

				while(!source_parsed[index].contains("(")){
					src = src + " "+source_parsed[index];
					++index;
				}

				String start = source_parsed[source_parsed.length-9].substring(0, source_parsed[source_parsed.length-9].length()-1);
				String end = parsed[parsed.length-3].substring(0, parsed[parsed.length-3].length()-1);

				if((Double.parseDouble(start)*1000000) > endFilter || (Double.parseDouble(end)*1000000) < startFilter) continue;
				else if((Double.parseDouble(start)*1000000) < startFilter) System.out.print("(Start Before Window)");
				else if((Double.parseDouble(end)*1000000) > endFilter) System.out.print("(Unfinished in Window)");

				System.out.print(src+", ========> ,"+dest + ", ; ");
				
				Double duration = (Double.parseDouble(end) - Double.parseDouble(start))*1000000;
				System.out.print("Timestamp: ;"+start+ "; to ;"+end+"; Takes: ");
				System.out.printf("%.0f\n", duration);

			}
			else continue;
		}
	}
}