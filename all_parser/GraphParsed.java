import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.util.Hashtable;

public class GraphParsed{
	public static void main(String []args) throws Exception{
		File file = new File(args[0]); //This file only contains the last level of the parse tree
		Scanner scanner = null;
		try{
			scanner = new Scanner(file);
		}
		catch(Exception e){
			System.err.println("File not found");
			System.exit(1);
		}
		long starttime = 0;
		long endtime = 0;
		long totalCPU = 0;

		ArrayList<String> activityManager = new ArrayList<String>();
		ArrayList<String> windowManager = new ArrayList<String>();
		ArrayList<String> parcel = new ArrayList<String>();
		ArrayList<String> notification = new ArrayList<String>();
		ArrayList<String> powerManager = new ArrayList<String>();
		ArrayList<String> choreographer = new ArrayList<String>();
		ArrayList<String> displayManager = new ArrayList<String>();
		ArrayList<String> threadLocal = new ArrayList<String>();
		ArrayList<String> contentService = new ArrayList<String>();
		ArrayList<String> alarmManager = new ArrayList<String>();

		long am = 0, wm = 0, pa = 0, nf = 0, pm = 0, ch = 0, dm = 0, tl = 0, cs = 0, alarm = 0, other = 0; 
		String parent = null;

		Scanner scanner2 = null;
		File file2 = new File(args[1]);//file 2 is the full parsed file
		try{
			scanner2 = new Scanner(file2);
		}
		catch(Exception e){
			System.err.println("File 2 not found.");
			System.exit(1);
		}

		long inclusive = 0;
		boolean category = false;
		boolean first = true;
		int num_cate = 0;
		Hashtable <String, Integer> collision = new Hashtable <String, Integer> ();
		ArrayList <String> fuck = new ArrayList<String>();
		String last = null;
		while(scanner2.hasNext()){
			String line = scanner2.nextLine();
			
			if(!(line.contains("(") && line.contains(")")) || line.contains("context switch") || line.contains("-Root")) continue;
			while(line.contains("  ")) line = line.replaceAll("  ", " ");
			
			if(line.contains("(1)")){
				if(!first && !category){
					other += inclusive;
					for(int i = 0; i < fuck.size(); i++){
						//System.out.println(fuck.get(i));
					}

					//System.out.println("\n\n\n\n\n");
				}

				fuck.clear();

				if(collision.size() > 1){
					/*System.out.println("---------------------");
					System.out.println("Binder serve 2!");
					System.out.println(line);
					System.out.println("------------------------");*/
				}
				if(first) first = false;
				inclusive = Long.parseLong(line.split(" ")[7]);
				category = false;
				collision.clear();
			}
			fuck.add(line);

			if(line.contains("com.android.server.am")){
				if(!category) am += inclusive;
				category = true;
				collision.put("am",0);
			}

			if(line.contains("com.android.server.wm")){
				if(!category) wm += inclusive;
				category = true;
				collision.put("wm",0);
			}

			if(line.contains("Notification")){
				if(!category) nf += inclusive;
				category = true;
				collision.put("nf",0);
			}

			if(line.contains("android.content.pm") || line.contains("PowerManager")){
				if(!category) pm += inclusive;
				category = true;
				collision.put("pm",0);
			}

			if(line.contains("Choreographer")){
				if(!category) ch += inclusive;
				category = true;
				collision.put("ch", 0);
			}

			if(line.contains("DisplayManager")){
				if(!category) dm += inclusive;
				category = true;
				collision.put("dm", 0);
			}

			if(line.contains("ContentService")){
				if(!category) cs += inclusive;
				category = true;
				collision.put("cs", 0);
			}

			if(line.contains("AlarmManager")){
				if(!category) alarm += inclusive;
				category = true;
				collision.put("alarm",0);
			}
			last = line;
			//System.out.println(inclusive);
		}

		while(scanner.hasNext()){
			String line = scanner.nextLine();

			if(line.contains("Filter Start time")){
				starttime = Long.parseLong(line.substring(19));
				//System.out.println("I am here!");
				continue;
			}
			else if(line.contains("Filter End time")){
				endtime = Long.parseLong(line.substring(17));
				continue;
			}

			if(!(line.contains("(") && line.contains(")")) || line.contains("context switch")) continue;

			while(line.contains("  ")) line = line.replaceAll("  ", " ");

			//System.out.println(line);
			String []parsed = line.split(" ");
			
			if(line.contains("-Root")){
				totalCPU += Long.parseLong(parsed[8]);
				continue;
			}

			if(line.contains("com.android.server.am")){
				//System.out.println(parsed[8]);
				activityManager.add(parsed[8]+" "+Long.parseLong(parsed[7]));
				//am += Long.parseLong(parsed[7]);
				continue;
			}

			if(line.contains("com.android.server.wm")){
				windowManager.add(parsed[8]+" "+Long.parseLong(parsed[7]));
				//wm += Long.parseLong(parsed[7]);
				continue;
			}

			if(line.contains("Notification")){
				notification.add(parsed[8]+" "+Long.parseLong(parsed[7]));
				//nf += Long.parseLong(parsed[7]);
				continue;
			}

			if(line.contains("PowerManager")){
				powerManager.add(parsed[8]+" "+Long.parseLong(parsed[7]));
				//pm += Long.parseLong(parsed[7]);
				continue;
			}

			if(line.contains("Choreographer")){
				choreographer.add(parsed[8]+" "+Long.parseLong(parsed[7]));
				//ch += Long.parseLong(parsed[7]);
				continue;
			}

			if(line.contains("DisplayManager")){
				displayManager.add(parsed[8]+" "+Long.parseLong(parsed[7]));
				//dm += Long.parseLong(parsed[7]);
				continue;
			}

			if(line.contains("ContentService")){
				contentService.add(parsed[8]+" "+Long.parseLong(parsed[7]));
				//cs += Long.parseLong(parsed[7]);
				continue;
			}

			if(line.contains("AlarmManager")){
				alarmManager.add(parsed[8]+" "+Long.parseLong(parsed[7]));
				//alarm += Long.parseLong(parsed[7]);
				continue;
			}
			//if(line.contains("(1)")) parent = line;

		}

		Hashtable <String, Double> nameCheck = new Hashtable<String, Double> ();
		Long timeWindow = endtime - starttime;

		Long cpuBusy = (long)1733351;

		System.out.printf("CPU time (due to system overhead): %.2f%%\n", (double)totalCPU/cpuBusy*100);
		System.out.println("CPU busy time of system server(us): "+totalCPU);

		//System.out.printf("CPU Idle time: %.2f\n", (double)(timeWindow - totalCPU)/timeWindow*100);
		double mis = 0;
		other = totalCPU;
		totalCPU = cpuBusy;
		System.out.println("---------------------------------------");
		System.out.printf("Activity Manager: %.2f%%\n", (double)am/totalCPU*100);
		mis = 0;

		for(int i = 0; i < activityManager.size(); i++){
			String temp = activityManager.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		String keys = nameCheck.toString();
		ArrayList <Double> value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/am*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/am))*100);
		nameCheck.clear();


		System.out.println("---------------------------------------");
		System.out.printf("Window Manager: %.2f%%\n", (double)wm/totalCPU*100);
		mis = 0;

		for(int i = 0; i < windowManager.size(); i++){
			String temp = windowManager.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/wm*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/wm))*100);
		nameCheck.clear();

		/*System.out.println("---------------------------------------");
		System.out.printf("Parcel: %.2f%%\n", (double)pa/totalCPU*100);
		mis = 0;

		for(int i = 0; i < parcel.size(); i++){
			String temp = parcel.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/pa*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/pa))*100);
		nameCheck.clear();*/

		System.out.println("---------------------------------------");
		System.out.printf("Notification: %.2f%%\n", (double)nf/totalCPU*100);
		mis = 0;

		for(int i = 0; i < notification.size(); i++){
			String temp = notification.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/nf*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/nf))*100);
		nameCheck.clear();

		System.out.println("---------------------------------------");
		System.out.printf("Power Manager: %.2f%%\n", (double)pm/totalCPU*100);
		mis = 0;

		for(int i = 0; i < powerManager.size(); i++){
			String temp = powerManager.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/pm*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/pm))*100);
		nameCheck.clear();

		System.out.println("---------------------------------------");
		System.out.printf("Choreographer: %.2f%%\n", (double)ch/totalCPU*100);
		mis = 0;

		for(int i = 0; i < choreographer.size(); i++){
			String temp = choreographer.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/ch*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/ch))*100);
		nameCheck.clear();

		System.out.println("---------------------------------------");
		System.out.printf("Display Manager: %.2f%%\n", (double)dm/totalCPU*100);
		mis = 0;

		for(int i = 0; i < displayManager.size(); i++){
			String temp = displayManager.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/dm*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/dm))*100);
		nameCheck.clear();

		/*System.out.println("---------------------------------------");
		System.out.printf("Thread Local: %.2f%%\n", (double)tl/totalCPU*100);
		mis = 0;

		for(int i = 0; i < threadLocal.size(); i++){
			String temp = threadLocal.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/tl*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/tl))*100);
		nameCheck.clear();*/

		System.out.println("---------------------------------------");
		System.out.printf("Content Service: %.2f%%\n", (double)cs/totalCPU*100);
		mis = 0;

		for(int i = 0; i < contentService.size(); i++){
			String temp = contentService.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/cs*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/cs))*100);
		nameCheck.clear();

		System.out.println("---------------------------------------");
		System.out.printf("Alarm Manager: %.2f%%\n", (double)alarm/totalCPU*100);
		mis = 0;

		for(int i = 0; i < alarmManager.size(); i++){
			String temp = alarmManager.get(i);
			mis += Double.parseDouble(temp.split(" ")[1]);
			String name = temp.split(" ")[0];
			double timeElapsed = Double.parseDouble(temp.split(" ")[1]);

			if(nameCheck.get(name) == null){
				nameCheck.put(name, timeElapsed);
			}
			else{
				timeElapsed += nameCheck.get(name);
				nameCheck.put(name, timeElapsed);
			}	
		}

		keys = nameCheck.toString();
		value = new ArrayList <Double> (nameCheck.values());

		for(int i = 0; i < value.size(); i++){
			String key_temp = keys.split(",")[i];
			System.out.printf("%s: %.2f%%\n", key_temp.substring(0,key_temp.indexOf("=")), value.get(i)/alarm*100);
		}
		
		System.out.printf("others: %.2f%%\n", (1-(mis/alarm))*100);
		nameCheck.clear();
		System.out.println("------------------------------------");
		other = other - am - wm - pa - nf - pm - ch - dm - tl - cs - alarm;
		System.out.printf("The rest: %.2f%%\n", (double)(other)/totalCPU * 100);
	}
}