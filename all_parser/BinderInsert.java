import java.util.Scanner;
import java.util.Hashtable;
import java.io.File;

public class BinderInsert{
	public static void main(String []args) throws Exception{
		File f1 = new File(args[0]); //Output parsed by traceview
		
		Scanner scanner1 = null;
		
		try{
			scanner1 = new Scanner(f1);
			
		}
		catch (Exception e){
			System.err.println("File not found");
			System.exit(1);
		}

		while(scanner1.hasNext()){
			String line = scanner1.nextLine();
			System.out.println(line);
			if(!line.contains("execTransact")){
				continue;
			}
			while(line.contains("  ")){
				line = line.replaceAll("  ", " ");
			}

			String []parsed = line.split(" ");
			parsed[1] = parsed[1].replaceAll("\\(Unfinished\\)", "");//.replaceAll("(start_before)","");
			int goalId = Integer.parseInt(parsed[1].substring(3));
			long filterStart = Long.parseLong(parsed[4]);
			long filterEnd = Long.parseLong(parsed[5]);
			//System.out.println(filterStart+" "+filterEnd);
			//System.out.println(goalId);
			File f2 = new File(args[1]); //output parse by BinderParser
			Scanner scanner2 = null;
			try{
				scanner2 = new Scanner(f2);
			}
			catch (Exception e){
				System.err.println("File not found");
				System.exit(1);
			}

			//System.out.println("------gggggggggggggggggggg------------");


			while(scanner2.hasNext()){
				String line1 = scanner2.nextLine();
				String parsed1[] = line1.split(";");
				Double start = Double.parseDouble(parsed1[2]);
				Double end = Double.parseDouble(parsed1[4]);

				//System.out.println(start+ " "+end);
				if(start*1000000 > filterEnd+500 || end*1000000 < filterStart-500) continue;

				
				String p2[] = parsed1[0].split(",");
				while(p2[0].contains(" ")){
					p2[0] = p2[0].replaceAll(" ", "");
				}

				while(p2[2].contains(" ")){
					p2[2] = p2[2].replaceAll(" ", "");
				}

				if(Integer.parseInt(p2[0].substring(p2[0].indexOf("-")+1)) == goalId || Integer.parseInt(p2[2].substring(p2[2].indexOf("-")+1)) == goalId){
					System.out.println(line1.replaceAll(","," ").replaceAll(";", " "));

				}

			}

		}
	}
}